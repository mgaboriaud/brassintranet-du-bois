import json

from django.urls import reverse_lazy
from jsonschema import validate

PREFIX_DROPDOWN_ID = 'dropdownNav'


def get_navbar_data(request, path_name, path_schema):
    """
    Récupère les données définies dans la configuration de la navbar (fichier JSON), les valide avec son schéma JSON
    associé, puis retourne un dictionnaire de données optimisé pour le rendering du template de la navbar
    @param request: objet HttpRequest depuis lequel est appelé la méthode (permet notamment de vérifier les permissions
        de l'utilisateur courant)
    @param path_name: chemin vers le fichier de configuration de la navbar
    @param path_schema: chemin vers le fichier schéma JSON définissant la configuration de la navbar
    @return: dictionnaire des données de la navbar
    """
    navbar_raw_data = get_json_from_path(path_name)
    navbar_schema = get_json_from_path(path_schema)

    # Validation du schéma JSON + unicité des noms des launchers
    validate_json_schema(navbar_raw_data, navbar_schema)
    names = [launcher_raw['nom'] for launcher_raw in navbar_raw_data['launchers']]
    while len(names) > 0:
        name = names.pop()
        if name in names:
            raise Exception("[BDB] Navbar definition error : launcher names must be unique")

    navbar_data = format_data_for_rendering(request, navbar_raw_data)
    return navbar_data


def format_data_for_rendering(request, navbar_raw_data):
    """
    Convertit les données brutes du ficher JSON des données de la navbar en un dictionnaire dont la structure est
    optimisée pour être facilement utilisable dans le template de la navbar ainsi qu'améliorer les performances du
    rendering. Tient compte des permissions utilisateur définies dans le fichier de données.
    @param request: objet HttpRequest contenant les permissions utilisateur à vérifier par rapport à la définition de
        la navbar
    @param navbar_raw_data: dictionnaire de données brutes récupérées depuis le fichier de données de la navbar
    @return: dictionnaire de données formatées pour le rendering
    """
    navbar_data = {
        'launchers': [],
        'dropdowns': []
    }
    for launcher_raw in navbar_raw_data['launchers']:
        if 'permissions' not in launcher_raw.keys() or validate_permissions(request, launcher_raw['permissions']):
            launcher = {
                'id': launcher_raw['id'],
                'nom': launcher_raw['nom'],
                'icone': launcher_raw['icone']
            }
            if 'url' in launcher_raw:
                launcher['opens_dropdown'] = False
                launcher['href'] = get_href_from_url(launcher_raw['url'])
            elif 'urls' in launcher_raw:
                launcher['opens_dropdown'] = True
                dropdown_id = PREFIX_DROPDOWN_ID + launcher_raw['nom']
                launcher['href'] = '#' + dropdown_id

                dropdown = {
                    'id': dropdown_id,
                    'urls': []
                }
                for url in launcher_raw['urls']:
                    if 'permissions' not in url.keys() or validate_permissions(request, url['permissions']):
                        dropdown['urls'].append({
                            "titre": url['titre'],
                            "href": get_href_from_url(url['url'])
                        })
                navbar_data['dropdowns'].append(dropdown)
            else:
                launcher['href'] = ""

            navbar_data['launchers'].append(launcher)

    return navbar_data


def get_href_from_url(url):
    """
    Méthode utilitaire permettant de générer une valeur à mettre dans l'attribut href d'un tag <a> à partir d'un objet
    URL de la configuration de la navbar. Pour un type "raw", le contenu de l'attribut "ref" sera retourné sans
    traitement. Pour un type "name", c'est l'URL référencée par le nom spécifié dans l'attribut "ref" qui sera retournée.
    @param url: dictionnaire URL de la forme {"type": "string", "ref": "string"} (détail de la spécification dans le
    schéma JSON)
    @return: string à affecter directement à un attribut href
    """
    if url['type'] == 'name':
        return reverse_lazy(url['ref'])
    elif url['type'] == 'raw':
        return url['ref']
    else:
        raise Exception("[BDB] Unauthorised URL type in navbar definition. JSON Schema validation probably failed...")


def validate_permissions(request, permissions):
    """
    Retourne True si l'utilisateur lié à la requête spécifiée dispose de l'une des permissions de la liste donnée en
    argument, False sinon.
    @param request: objet HttpRequest auquel est lié l'utilisateur dont on veut vérifier les permissions
    @param permissions: liste de permission strings au format <app_name>.<permission_codename> de permissions à vérifier
    @return: booléen déterminant si l'utilisateur lié à request dispose de l'une des permissions spécifiées
    """
    for permission in permissions:
        if request.user.has_perm(permission):
            return True
    return False


def validate_json_schema(data, schema):
    """
    Vérifie la structure d'un dictionnaire en fonction d'un schéma JSON. Remonte une erreur si cette structure ne
    correspond pas à ce qui est défini dans le schéma.
    @param data: dictionnaire de données que l'on souhaite valider
    @param schema: dictionnaire suivant un modèle de schéma JSON et permettant la validation du dictionnaire `data`
    @raise Exception: exception remontée si la structure de `data` ne correspond pas au schéma
    """
    try:
        validate(data, schema)
    except Exception as e:
        raise Exception("[BDB] Error detected while parsing the navbar data file :" + str(e))


def get_json_from_path(path):
    """
    Retourne le dictionnaire Python issu d'un fichier JSON
    @param path: string représentant le chemin du fichier JSON
    @return: dictionnaire contenant les données du fichier JSON
    """
    file_obj = open(path, 'r')
    dict = json.load(file_obj)
    return dict
