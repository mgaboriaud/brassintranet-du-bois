from time import time, sleep

DEFAULT_STEP = 0.1


def await_condition(condition, timeout):
    """
    Met en pause l'exécution du code tant que la condition n'est pas remplie. Lorsque le timeout est dépassé,
    l'exécution du code reprend son cours normal.
    @param condition: booléen, condition à remplir pour faire repartir l'exécution
    @param timeout: nombre entier, temps en secondes à attendre avant de reprendre l'exécution même si condition = False
    @return: True lorsque condition = True, False lorsque la condition n'a toujours pas été remplie au bout du timeout
    """
    time_start = time()
    while not condition:
        if time() - time_start < timeout:
            sleep(DEFAULT_STEP)
        else:
            return False
    return True