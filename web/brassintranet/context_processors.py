from django.contrib import messages

from brassintranet.service.navbar_data_service import get_navbar_data, get_json_from_path
from brassintranet.settings import BASE_DIR, STATIC_URL

PATH_VERSION = str(BASE_DIR) + '/version.json'

PATH_CONF_NAVBAR_DATA = str(BASE_DIR) + STATIC_URL + 'data/navbar.json'
PATH_CONF_NAVBAR_SCHEMA = str(BASE_DIR) + STATIC_URL + 'data/navbar-schema.json'


def get_universal_context_variables(request):
    universal_context_vars = {}

    # Dictionnaire niveau de message / set de alert classes Bootstrap
    universal_context_vars['bootstrap_message_classes'] = {
        messages.DEFAULT_LEVELS['INFO']: "alert alert-info",
        messages.DEFAULT_LEVELS['SUCCESS']: "alert alert-success",
        messages.DEFAULT_LEVELS['WARNING']: "alert alert-warning",
        messages.DEFAULT_LEVELS['ERROR']: "alert alert-danger",
    }

    return universal_context_vars


def load_version(request):
    if request.user.is_authenticated:
        # Récupération des données de version seulement si une nouvelle session vient d'être chargée
        # Permet d'éviter de recalculer ces données à chaque changement de page
        if 'version' in request.session:
            version = request.session['version']
        else:
            version = get_json_from_path(PATH_VERSION)
            request.session['version'] = version

        return {"version": version}
    else:
        return {}


def load_navbar_data(request):
    if request.user.is_authenticated:
        # Récupération des données de la navbar seulement si une nouvelle session vient d'être chargée
        # Permet d'éviter de recalculer ces données à chaque changement de page
        if 'navbar_data' in request.session:
            navbar_data = request.session['navbar_data']
        else:
            navbar_data = get_navbar_data(request, PATH_CONF_NAVBAR_DATA, PATH_CONF_NAVBAR_SCHEMA)
            request.session['navbar_data'] = navbar_data

        return {"navbar_data": navbar_data}
    else:
        return {}
