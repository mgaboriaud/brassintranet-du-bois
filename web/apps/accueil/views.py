from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render
from django.views.decorators.cache import cache_page

from apps.users.tasks import send_async_email


@staff_member_required(login_url=settings.LOGIN_URL)
def index(request):
    context_vars = {}
    return render(request, 'accueil/index.html', context_vars)