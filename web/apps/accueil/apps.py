from django.apps import AppConfig


class AccueilConfig(AppConfig):
    name = 'apps.accueil'
