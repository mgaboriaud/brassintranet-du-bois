from django.core.management.base import BaseCommand
from apps.users.models import UserExtended


class Command(BaseCommand):
    help = "Force la fermeture de toutes les sessions utilisateurs, et ainsi leur déconnexion."

    def handle(self, *args, **options):
        print("Ending all user sessions...")
        for user in UserExtended.objects.all():
            user.force_logout = True
            user.save()
        print("All user sessions successfully ended.")
