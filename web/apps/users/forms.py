from django import forms
from django.contrib.auth.forms import UserCreationForm

from apps.users.models import UserExtended


class LoginForm(forms.Form):
    username_email = forms.CharField(
        label="Nom d'utilisateur ou adresse mail",
        max_length=150
    )
    password = forms.CharField(
        label="Mot de passe",
        widget=forms.PasswordInput
    )


class ProfilForm(forms.ModelForm):
    class Meta:
        model = UserExtended
        fields = ('username', 'first_name', 'last_name', 'email')


class CouleurForm(forms.ModelForm):
    class Meta:
        model = UserExtended
        fields = ('couleur',)


class TokenSendForm(forms.Form):
    key = forms.CharField(
        label="Veuillez entrer la clé de votre jeton d'inscription",
        max_length=32
    )


class InscriptionForm(UserCreationForm):
    class Meta:
        model = UserExtended
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Surcharge du champ username pour interdire le caractère "@"
        self.fields['username'].help_text = "Votre nom d'utilisateur doit comporter moins de 150 caractères (lettres " \
                                            "majuscules et minuscules, chiffres, \"+\", \"-\", \"_\")"

    def clean(self):
        """
        Nettoyage du champ username.
        Le caractère "@" est désormais interdit dans le nom d'utilisateur.
        """
        cleaned_data = super().clean()
        username = cleaned_data['username']
        first_name = cleaned_data['first_name']
        if username.find('@') > -1:
            self.add_error('username', "Le caractère \"@\" est interdit pour les noms d'utilisateurs")
        if username.lower() == first_name.lower():
            self.add_error('username', "Ce surnom est claqué au sol. Veuillez en choisir un autre svp")
