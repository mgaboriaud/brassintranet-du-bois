from __future__ import absolute_import, unicode_literals

from celery import shared_task
from django.core.mail import send_mail

from brassintranet.settings import EMAIL_HOST_USER


@shared_task
def send_async_email(title, body, recipient_list):
    send_mail(title, body, EMAIL_HOST_USER, recipient_list)
    return None


@shared_task
def send_async_email_html(title, html_body, recipient_list):
    message_if_template_load_error = "Une erreur est survenue lors de l'envoi du mail, veuillez contacter le support."
    send_mail(title, message_if_template_load_error, EMAIL_HOST_USER, recipient_list, fail_silently=True, html_message=html_body)
    return None
