import time
from datetime import timedelta

from bs4 import BeautifulSoup
from celery.contrib.testing.worker import start_worker
from django.contrib.auth import get_user
from django.contrib.auth.models import Permission
from django.contrib.messages import get_messages
from django.core import mail
from django.test import TestCase, SimpleTestCase
from django.urls import reverse_lazy
from django.utils import timezone

from brassintranet.celery import app
from brassintranet.service.test_utils_service import await_condition
from brassintranet.settings import LOGIN_REDIRECT_URL
from .forms import ProfilForm, CouleurForm
from .middleware import MESSAGE_WARNING_FORCED_LOGOUT
from .models import UserCreationToken, UserExtended, generate_token_key
from .tasks import send_async_email, send_async_email_html
from .views import MESSAGE_ERROR_WRONG_CREDENTIALS, MESSAGE_ERROR_WRONG_TOKEN_KEY, MESSAGE_SUCCESS_TOKEN_KEY_OK, \
    MESSAGE_ERROR_SIGNUP_FIELD_ERRORS, MESSAGE_SUCCESS_SIGNUP_OK, MESSAGE_SUCCESS_PROFIL_CHANGE_OK, \
    MESSAGE_ERROR_PROFIL_FIELD_ERRORS

# Pour lancer les tests : docker-compose run --rm web python manage.py test


### Tests sur les vues


class UserLoginViewTestCase(TestCase):
    """
    Règles d'entrée de chaque Test Case :
    - Il existe un unique utilisateur test dont les identifiants sont définis par les constantes de classe USERNAME,
        EMAIL et PASSWORD
    """

    URL_NAME = 'users:login'

    USERNAME = 'test'
    EMAIL = 'test@test.test'
    PASSWORD = '000000'

    FAKE_USERNAME = 'fake'
    FAKE_EMAIL = 'fake@fake.fake'
    FAKE_PASSWORD = '111111'

    def setUp(self) -> None:
        # Créer l'utilisateur test
        UserExtended.objects.create_user(self.USERNAME, self.EMAIL, self.PASSWORD)

    def tearDown(self) -> None:
        # Supprimer l'utilisateur test
        user = UserExtended.objects.get(username=self.USERNAME)
        user.delete()

    def test_user_login_redirect_if_authenticated(self):
        """
        Test Case : si l'utilisateur est connecté, une entrée sur la page de login doit le rediriger vers la page
        d'accueil
        > Vérifie qu'une redirection vers la page de redirection par défaut a bien lieu
        """
        self.client.login(username=self.USERNAME, password=self.PASSWORD)

        response = self.client.get(reverse_lazy(self.URL_NAME))
        self.assertRedirects(response, LOGIN_REDIRECT_URL, fetch_redirect_response=False)

        self.client.logout()

    def test_user_login_page_with_empty_form_if_get(self):
        """
        Test Case : le chargement de cette vue avec une requête GET doit correctement charger le formulaire de connexion
        > Vérifie que la page se charge avec un code 200
        """
        response = self.client.get(reverse_lazy(self.URL_NAME))
        self.assertEqual(response.status_code, 200)

    def test_user_login_form_isnt_valid(self):
        """
        Test Case : si le formulaire envoyé ne respecte pas les règles de validation, on recharge la page de login avec
        un message d'erreur et les données déjà entrées (non-vérifié dans ce test).
        > Vérifie que le formulaire n'est pas valide
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message d'erreur affiché sur la page est le bon
        """
        response = self.client.post(reverse_lazy(self.URL_NAME), {
            'username_email': '',
            'password': ''
        })
        form = response.context['form']

        self.assertFalse(form.is_valid())
        self.assertEqual(response.status_code, 200)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_ERROR_WRONG_CREDENTIALS, messages)

    def test_user_login_username_or_email_doesnt_exist(self):
        """
        Test Case : si l'identifiant est inexistant, on recharge la page de login avec un message d'erreur
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message d'erreur affiché sur la page est le bon
        """
        response = self.client.post(reverse_lazy(self.URL_NAME), {
            'username_email': self.FAKE_USERNAME,
            'password': self.FAKE_PASSWORD # Mdp non-existant mais qui passe les règles de validation
        })

        self.assertEqual(response.status_code, 200)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_ERROR_WRONG_CREDENTIALS, messages)

    def test_user_login_username_ok_password_wrong(self):
        """
        Test Case : si l'identifiant existe mais que le mot de passe est erroné, on recharge la page de login avec un
        message d'erreur
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message d'erreur affiché sur la page est le bon
        """
        response = self.client.post(reverse_lazy(self.URL_NAME), {
            'username_email': self.USERNAME,
            'password': self.FAKE_PASSWORD
        })

        self.assertEqual(response.status_code, 200)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_ERROR_WRONG_CREDENTIALS, messages)

    def test_user_login_credentials_ok_username(self):
        """
        Test Case : donner un couple username/password existant doit authentifier l'utilisateur correspondant et le
        rediriger vers la page de redirection par défaut
        > Vérifie qu'un utilisateur est connecté
        > Vérifie que son username est le bon
        > Vérifie qu'une redirection vers la page de redirection par défaut a bien lieu
        """
        response = self.client.post(reverse_lazy(self.URL_NAME), {
            'username_email': self.USERNAME,
            'password': self.PASSWORD
        })
        user = get_user(self.client)

        self.assertTrue(user.is_authenticated)
        self.assertEqual(user.username, self.USERNAME)
        self.assertRedirects(response, LOGIN_REDIRECT_URL, fetch_redirect_response=False)

    def test_user_login_email_ok_password_wrong(self):
        """
        Test Case : si l'email existe mais que le mot de passe est erroné, on recharge la page de login avec un
        message d'erreur
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message d'erreur affiché sur la page est le bon
        """
        response = self.client.post(reverse_lazy(self.URL_NAME), {
            'username_email': self.EMAIL,
            'password': self.FAKE_PASSWORD
        })

        self.assertEqual(response.status_code, 200)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_ERROR_WRONG_CREDENTIALS, messages)

    def test_user_login_credentials_ok_email(self):
        """
        Test Case : donner un couple email/password existant doit authentifier l'utilisateur correspondant et le
        rediriger vers la page de redirection par défaut
        > Vérifie qu'un utilisateur est connecté
        > Vérifie que son email est le bon
        > Vérifie qu'une redirection vers la page de redirection par défaut a bien lieu
        """
        response = self.client.post(reverse_lazy(self.URL_NAME), {
            'username_email': self.EMAIL,
            'password': self.PASSWORD
        })
        user = get_user(self.client)

        self.assertTrue(user.is_authenticated)
        self.assertEqual(user.email, self.EMAIL)
        self.assertRedirects(response, LOGIN_REDIRECT_URL, fetch_redirect_response=False)


class UserPanelViewTestCase(TestCase):
    """
    Règles d'entrée de chaque Test Case :
    - Il existe un unique utilisateur test dont les identifiants sont définis par les constantes de classe USERNAME,
        EMAIL et PASSWORD
    - L'utilisateur défini par USERNAME est connecté
    """

    URL_NAME = 'users:profil_post'
    URL_ARG_PROFIL = 'profil'
    URL_ARG_COULEUR = 'couleur'
    URL_ARG_UNKNOWN = 'croziflette'

    USERNAME = 'staff'
    WRONG_USERNAME = 'staff staff'
    FIRST_NAME = 'Staff'
    LAST_NAME = 'Staffer'
    NEW_USERNAME = '574ff'
    EMAIL = 'staff@staff.staff'
    WRONG_EMAIL = 'staff12345'
    PASSWORD = '000000'

    def setUp(self) -> None:
        # Créer l'utilisateur test et se connecter avec ses identifiants
        user = UserExtended.objects.create_user(self.USERNAME, self.EMAIL, self.PASSWORD)
        user.first_name = self.FIRST_NAME
        user.last_name = self.LAST_NAME
        user.is_staff = True
        user.save()
        self.client.login(username=self.USERNAME, password=self.PASSWORD)

    def tearDown(self) -> None:
        # Se déconnecter et supprimer l'utilisateur test
        self.client.logout()
        user = UserExtended.objects.get(email=self.EMAIL)
        user.delete()

    def test_user_panel_post_profil_parameter(self):
        """
        Test Case : si on post avec paramètre "profil", on doit récupérer une instance ProfilForm
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le formulaire récupéré dans la vue est bien de type ProfilForm
        """
        response = self.client.post(reverse_lazy(self.URL_NAME, args=[self.URL_ARG_PROFIL]))
        self.assertEqual(response.status_code, 200)

        form = response.context['form_out']
        self.assertIsInstance(form, cls=ProfilForm)

    def test_user_panel_post_couleur_parameter(self):
        """
        Test Case : si on post avec paramètre "couleur", on doit récupérer une instance CouleurForm
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le formulaire récupéré dans la vue est bien de type ProfilForm
        """
        response = self.client.post(reverse_lazy(self.URL_NAME, args=[self.URL_ARG_COULEUR]))
        self.assertEqual(response.status_code, 200)

        form = response.context['form_out']
        self.assertIsInstance(form, cls=CouleurForm)

    def test_user_panel_post_unknown_parameter(self):
        """
        Test Case : si on post avec un paramètre non reconnu, 404
        > Vérifie que la requête retourne une erreur 404
        """
        response = self.client.post(reverse_lazy(self.URL_NAME, args=[self.URL_ARG_UNKNOWN]))
        self.assertEqual(response.status_code, 404)

    def test_user_panel_form_valid_check_user_info(self):
        """
        Test Case : si le formulaire est valide (test avec changement du username), les infos utilisateurs
        doivent avoir changé en base
        > Vérifie en base que le username de l'utilisateur courant a été modifié avec la bonne valeur
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message de succès affiché sur la page est le bon
        """
        user = get_user(self.client)
        response = self.client.post(reverse_lazy(self.URL_NAME, args=[self.URL_ARG_PROFIL]), {
            'username': self.NEW_USERNAME,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
        })
        fetched_user = UserExtended.objects.get(email=user.email)

        self.assertEqual(fetched_user.username, self.NEW_USERNAME)
        self.assertEqual(response.status_code, 200)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_SUCCESS_PROFIL_CHANGE_OK, messages)

    def test_user_panel_form_invalid_check_form_values(self):
        """
        Test Case : si le formulaire n'est pas valide, les erreurs doivent être transmises
        > Vérifie que le username de l'utilisateur courant n'a pas été modifié en base
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message de succès affiché sur la page est le bon
        > (2) Vérifie que les champs username et email présentent une erreur
        """
        user = get_user(self.client)
        response = self.client.post(reverse_lazy(self.URL_NAME, args=[self.URL_ARG_PROFIL]), {
            'username': self.WRONG_USERNAME,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': self.WRONG_EMAIL,
        })
        fetched_user = UserExtended.objects.get(email=user.email)
        form = response.context['form_profil']

        self.assertEqual(fetched_user.username, user.username)

        self.assertEqual(response.status_code, 200)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_ERROR_PROFIL_FIELD_ERRORS, messages)

        errors = form.errors.as_data()
        self.assertIn('username', errors.keys())
        self.assertIn('email', errors.keys())


class AsyncInscriptionViewTestCase(SimpleTestCase):
    """
    Règles d'entrée de chaque Test Case :
    - Il existe un unique utilisateur test dont les identifiants sont définis par les constantes de classe
    EXISTING_USERNAME, EXISTING_EMAIL et EXISTING_PASSWORD
    - La table des jetons dispose d'un unique jeton initialisé avec la clé KEY et valide pendant 24h
    - Aucun utilisateur n'est authentifié
    """

    databases = '__all__'

    URL_NAME = 'users:inscription'

    EXISTING_USERNAME = 'johndoe'
    EXISTING_EMAIL = 'john@doe.com'
    EXISTING_PASSWORD = '123456'

    USERNAME = 'spooderman'
    FIRST_NAME = 'Test'
    LAST_NAME = 'Tester'
    EMAIL = 'test@test.test'
    PASSWORD = 'aC0mpl3xP4ssw0rd'
    WRONG_PASSWORD = 'aWr0ngP4ssw0rd'

    KEY_PLACEHOLDER = "Th15iSasUp3rSecUReT0kEnk3y1234"

    # Démarrage du worker celery pour tester les tâches asynchrones
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Start up celery worker
        cls.celery_worker = start_worker(app, perform_ping_check=False, loglevel='FATAL')
        cls.celery_worker.__enter__()

    # Arrêt du worker celery
    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

        # Close worker
        cls.celery_worker.__exit__(None, None, None)

    def setUp(self) -> None:
        # Créer l'utilisateur test
        user = UserExtended.objects.create_user(self.EXISTING_USERNAME, self.EXISTING_EMAIL, self.EXISTING_PASSWORD)

        # Créer un jeton qui expire 24h après sa création (supérieur au timeout du test pour garantir sa validité)
        token = UserCreationToken.objects.create(recipient_email=user.email)
        token.date_expiration = timezone.now() + timedelta(days=1)
        token.save()

    def tearDown(self) -> None:
        # Supprimer l'utilisateur test
        user = UserExtended.objects.get(username=self.EXISTING_USERNAME)
        user.delete()

        # Supprimer tous les jetons en base
        for token in UserCreationToken.objects.all():
            token.delete()

        # Se déconnecter si un utilisateur est authentifié
        self.client.logout()

    def test_inscription_raise_404_if_no_token(self):
        """
        Test Case : une entrée sur la page de token doit retourner une erreur 404 si aucun token n'existe en base
        > Vérifie que la page se charge avec un code 404
        """
        # Supprimer tous les jetons en base
        for token in UserCreationToken.objects.all():
            token.delete()

        response = self.client.get(reverse_lazy(self.URL_NAME, args=[self.KEY_PLACEHOLDER]))
        self.assertEqual(response.status_code, 404)

    def test_inscription_redirect_if_authenticated(self):
        """
        Test Case : si l'utilisateur est connecté, une entrée sur la page d'inscription doit le rediriger vers la page
        d'accueil
        > Vérifie qu'une redirection vers la page de redirection par défaut a bien lieu
        """
        self.client.login(username=self.EXISTING_USERNAME, password=self.EXISTING_PASSWORD)

        response = self.client.get(reverse_lazy(self.URL_NAME, args=[self.KEY_PLACEHOLDER]))
        self.assertRedirects(response, LOGIN_REDIRECT_URL, fetch_redirect_response=False)

    def test_inscription_wrong_token_key(self):
        """
        Test Case : si l'on donne une clé de jeton non-existante, la page de "mauvais jeton" se charge avec un message
        d'erreur
        > Vérifie que le template chargé est bien la page d'erreur de clé de jeton
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message d'erreur affiché sur la page est le bon
        """
        response = self.client.get(reverse_lazy(self.URL_NAME, args=[self.KEY_PLACEHOLDER]))

        self.assertTemplateUsed(response, 'users/token_fail.html')
        self.assertEqual(response.status_code, 200)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_ERROR_WRONG_TOKEN_KEY, messages)

    def test_inscription_token_key_success(self):
        """
        Test Case : si la clé du jeton d'inscription que l'on a donnée est valide, le formulaire d'inscription doit se
        charger correctement avec un message de succès
        > Vérifie que le template chargé est bien celui du formulaire d'inscription
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le champ email du formulaire a la bonne valeur (email de réception du mail d'inscription)
        > Vérifie que le message de succès affiché sur la page est le bon
        """
        await_condition(len(mail.outbox) > 0, 10)
        email = mail.outbox[0].message().as_string()
        soup = BeautifulSoup(email)
        key = soup.body.a['href'].split('/')[-1]
        token = UserCreationToken.objects_unexpired.retrieve_token_with_key(key, True)

        response = self.client.get(reverse_lazy(self.URL_NAME, args=[key]))
        form = response.context['form']

        self.assertTemplateUsed(response, 'users/inscription.html')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(form['email'].value(), token.recipient_email)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_SUCCESS_TOKEN_KEY_OK, messages)

    def test_inscription_form_isnt_valid(self):
        """
        Test Case : la page d'inscription doit se recharger avec un message d'erreur si les champs sont erronés (3 cas
        ici vérifiés : confirmation mdp non similaire, '@' dans le username, email d'un utilisateur existant)
        > Vérifie que le template chargé est bien celui du formulaire d'inscription
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message d'erreur affiché sur la page est le bon
        """
        await_condition(len(mail.outbox) > 0, 10)
        email = mail.outbox[0].message().as_string()
        soup = BeautifulSoup(email)
        key = soup.body.a['href'].split('/')[-1]

        response_list = []
        base_post_params = {
            'key': key,
            'username': self.USERNAME,
            'first_name': self.FIRST_NAME,
            'last_name': self.LAST_NAME,
            'email': self.EMAIL,
            'password1': self.PASSWORD,
            'password2': self.PASSWORD
        }

        # Cas où les deux mdp sont différents
        post_params_wrong_password = base_post_params.copy()
        post_params_wrong_password['password2'] = self.WRONG_PASSWORD
        response_list.append(self.client.post(reverse_lazy(self.URL_NAME, args=[key]), post_params_wrong_password))

        # Cas où il y a un '@' dans le champ username
        post_params_forbidden_username = base_post_params.copy()
        post_params_forbidden_username['username'] += '@'
        response_list.append(self.client.post(reverse_lazy(self.URL_NAME, args=[key]), post_params_forbidden_username))

        # Cas où l'email donné existe déjà pour un utilisateur
        post_params_email_already_exists = base_post_params.copy()
        post_params_email_already_exists['email'] = self.EXISTING_EMAIL
        response_list.append(self.client.post(reverse_lazy(self.URL_NAME, args=[key]), post_params_email_already_exists))

        for response in response_list:
            self.assertTemplateUsed(response, 'users/inscription.html')
            self.assertEqual(response.status_code, 200)
            messages = [m.message for m in get_messages(response.wsgi_request)]
            self.assertIn(MESSAGE_ERROR_SIGNUP_FIELD_ERRORS, messages)

    def test_inscription_signup_success(self):
        """
        Test Case : si les champs du formulaire sont corrects, on valide l'inscription
        > Vérifie que l'utilisateur a été créé avec le bon username
        > Vérifie que le token créé pour l'inscription de cet utilisateur a bien été supprimé
        > Vérifie que le template chargé est bien celui de la page de succès d'inscription
        > Vérifie que la page se charge avec un code 200
        > Vérifie que le message de succès affiché sur la page est le bon
        > Vérifie qu'un email a bien été envoyé et que le destinataire est le bon
        """
        await_condition(len(mail.outbox) > 0, 10)
        email = mail.outbox[0].message().as_string()
        soup = BeautifulSoup(email)
        key = soup.body.a['href'].split('/')[-1]

        response = self.client.post(reverse_lazy(self.URL_NAME, args=[key]), {
            'key': key,
            'username': self.USERNAME,
            'first_name': self.FIRST_NAME,
            'last_name': self.LAST_NAME,
            'email': self.EMAIL,
            'password1': self.PASSWORD,
            'password2': self.PASSWORD
        })

        self.assertEqual(len(UserExtended.objects.filter(username=self.USERNAME)), 1)
        user = UserExtended.objects.filter(username=self.USERNAME)

        self.assertEqual(UserCreationToken.objects_unexpired.retrieve_token_with_key(key, True), None)

        self.assertTemplateUsed(response, 'users/notification_and_homebutton.html')
        self.assertEqual(response.status_code, 200)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_SUCCESS_SIGNUP_OK, messages)

        self.assertTrue(await_condition(len(mail.outbox) > 0, 10))
        self.assertEqual(len(mail.outbox[-1].to), 1)
        self.assertEqual(mail.outbox[-1].to[0], self.EMAIL)

        # Suppression de l'utilisateur pour éviter les données parasites dans les autres tests
        user.delete()


### Tests sur les gestionnaires d'objets


class AsyncUserCreationTokenUnexpiredManagerTestCase(SimpleTestCase):
    """
    Règles d'entrée de chaque Test Case :
    - La table des jetons d'inscription est vide
    """

    databases = '__all__'

    EMAIL = 'test@test.test'

    # Démarrage du worker celery pour tester les tâches asynchrones
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Start up celery worker
        cls.celery_worker = start_worker(app, perform_ping_check=False)
        cls.celery_worker.__enter__()

    # Arrêt du worker celery
    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

        # Close worker
        cls.celery_worker.__exit__(None, None, None)

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        # Supprimer tous les jetons en base
        for token in UserCreationToken.objects.all():
            token.delete()

    def test_token_key_is_none(self):
        """
        Test Case : après la création d'un jeton en base, celui-ci doit avoir sa valeur key à None (valeur supprimée
        pour des raisons de sécurité)
        > Vérifie que la clé du jeton créé est bien None
        """
        token = UserCreationToken.objects.create()
        self.assertIsNone(token.key)

    def test_get_queryset_no_expired_token(self):
        """
        Test Case : si un jeton expiré existe en base, il ne doit pas être retourné par le QuerySet du gestionnaire
        Unexpired.
        > Vérifie la non existence d'un token créé avec date d'expiration passée dans le QuerySet
        """
        # Création d'un jeton qui expire immédiatement
        token = UserCreationToken.objects.create()
        token.date_expiration = timezone.now()
        token.save()

        self.assertNotIn(token, UserCreationToken.objects_unexpired.all())

    def test_retrieve_token_with_key_finds_a_token(self):
        """
        Test Case : si un jeton non expiré et créé avec une clé key existe en base, la méthode doit retourner ce token
        à partir de key
        > Vérifie l'existence de retrieve_token_with_key(key, True) après avoir créé un token et récupéré key
        directement dans le mail envoyé et récupéré dans l'outbox
        """
        # Création d'un jeton qui expire 24h après sa création (supérieur au timeout du test pour garantir sa validité)
        token = UserCreationToken.objects.create(recipient_email=self.EMAIL)
        token.date_expiration = timezone.now() + timedelta(days=1)
        token.save()

        time.sleep(1)
        email = mail.outbox[0].message().as_string()
        soup = BeautifulSoup(email)
        key = soup.body.a['href'].split('/')[-1]

        self.assertEqual(token, UserCreationToken.objects_unexpired.retrieve_token_with_key(key, True))


### Tests sur les signaux


class AsyncNotifyStaffOnNewStaffOrAdminUserSignalTestCase(SimpleTestCase):
    """
    Règles d'entrée de chaque Test Case :
    - Il existe exactement 2 utilisateurs membres du staff dont les noms d'utilisateur sont désignés par
    USERNAME_STAFF1 et USERNAME_STAFF2
    - La boîte mail virtuelle est vide
    """
    databases = '__all__'

    USERNAME = 'test'
    EMAIL = 'test@test.test'
    PASSWORD = '000000'

    USERNAME_STAFF1 = 'staff1'
    EMAIL_STAFF1 = 'staff1@staff.staff'
    PASSWORD_STAFF1 = '111111'

    USERNAME_STAFF2 = 'staff2'
    EMAIL_STAFF2 = 'staff2@staff.staff'
    PASSWORD_STAFF2 = '222222'

    # Démarrage du worker celery pour tester les tâches asynchrones
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Start up celery worker
        cls.celery_worker = start_worker(app, perform_ping_check=False)
        cls.celery_worker.__enter__()

    # Arrêt du worker celery
    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

        # Close worker
        cls.celery_worker.__exit__(None, None, None)

    def setUp(self) -> None:
        UserExtended.objects.create_user(self.USERNAME_STAFF1, self.EMAIL_STAFF1, self.PASSWORD_STAFF1)
        staff1 = UserExtended.objects.get(username=self.USERNAME_STAFF1)
        staff1.is_staff = True
        staff1.save()

        UserExtended.objects.create_user(self.USERNAME_STAFF2, self.EMAIL_STAFF2, self.PASSWORD_STAFF2)
        staff2 = UserExtended.objects.get(username=self.USERNAME_STAFF2)
        staff2.is_staff = True
        staff2.save()

        mail.outbox = []

    def tearDown(self) -> None:
        staff1 = UserExtended.objects.get(username=self.USERNAME_STAFF1)
        staff1.delete()

        staff2 = UserExtended.objects.get(username=self.USERNAME_STAFF2)
        staff2.delete()

    def test_update_staff_notify_staff(self):
        """
        Test Case : si on met à True le statut is_staff d'un utilisateur, alors mail envoyé à tous les membres du staff
        > Vérifie qu'au moins un mail a été envoyé à la suite de la mise à True du is_staff d'un utilisateur courant
        > Vérifie que les deux utilisateurs du staff ont bien leur mail présent dans les destinataires du mail de
        notification
        """
        UserExtended.objects.create_user(self.USERNAME, self.EMAIL, self.PASSWORD)
        user = UserExtended.objects.get(username=self.USERNAME)
        user.is_staff = True
        user.save()

        self.assertTrue(await_condition(len(mail.outbox) > 0, 10))
        recipients = mail.outbox[-1].to
        self.assertIn(self.EMAIL_STAFF1, recipients)
        self.assertIn(self.EMAIL_STAFF2, recipients)

        user.delete()

    def test_update_superuser_notify_staff(self):
        """
        Test Case : si on met à True le statut is_superuser d'un utilisateur, alors mail envoyé à tous les membres du staff
        > Vérifie qu'au moins un mail a été envoyé à la suite de la mise à True du is_superuser d'un utilisateur courant
        > Vérifie que les deux utilisateurs du staff ont bien leur mail présent dans les destinataires du mail de
        notification
        """
        UserExtended.objects.create_user(self.USERNAME, self.EMAIL, self.PASSWORD)
        user = UserExtended.objects.get(username=self.USERNAME)
        user.is_superuser = True
        user.save()

        self.assertTrue(await_condition(len(mail.outbox) > 0, 10))
        recipients = mail.outbox[-1].to
        self.assertIn(self.EMAIL_STAFF1, recipients)
        self.assertIn(self.EMAIL_STAFF2, recipients)

        user.delete()

    def test_create_superuser_notify_staff(self):
        """
        Test Case : si on crée un nouvel utilisateur superuser, alors mail envoyé à tous les membres du staff
        > Vérifie qu'au moins un mail a été envoyé à la suite de la création du nouveau superuser
        > Vérifie que les deux utilisateurs du staff ont bien leur mail présent dans les destinataires du mail de
        notification
        """
        UserExtended.objects.create_superuser(self.USERNAME, self.EMAIL, self.PASSWORD)

        self.assertTrue(await_condition(len(mail.outbox) > 0, 10))
        recipients = mail.outbox[-1].to
        self.assertIn(self.EMAIL_STAFF1, recipients)
        self.assertIn(self.EMAIL_STAFF2, recipients)

        user = UserExtended.objects.get(username=self.USERNAME)
        user.delete()


class EnableForceLogoutOnPermissionChangeSignalTestCase(TestCase):
    """
    Règles d'entrée de chaque Test Case :
    - Il existe un unique utilisateur test dont les identifiants sont définis par les constantes de classe USERNAME,
        EMAIL et PASSWORD
    - Cet utilisateur a son attribut force_logout à False
    - Cet utilisateur dispose de la permission add_logentry
    """

    USERNAME = 'test'
    EMAIL = 'test@test.test'
    PASSWORD = '000000'

    def setUp(self) -> None:
        # Créer l'utilisateur test
        UserExtended.objects.create_user(self.USERNAME, self.EMAIL, self.PASSWORD)
        user = UserExtended.objects.get(username=self.USERNAME)
        permission = Permission.objects.get(codename='add_logentry')
        user.user_permissions.add(permission)
        user.force_logout = False
        user.save()

    def tearDown(self) -> None:
        # Supprimer l'utilisateur test
        user = UserExtended.objects.get(username=self.USERNAME)
        user.delete()

    def test_delete_user_permission_enable_force_logout(self):
        """
        Test Case : si on supprime une permission à un utilisateur, son attribut force_logout doit passer à True
        > Vérifie que le force_logout de l'utilisateur est bien à True après suppression d'une de ses permissions
        """
        user = UserExtended.objects.get(username=self.USERNAME)
        permission = Permission.objects.get(codename='add_logentry')
        user.user_permissions.remove(permission)

        self.assertTrue(user.force_logout)


class DisableForceLogoutOnConnectionSignalTestCase(TestCase):
    """
    Règles d'entrée de chaque Test Case :
    - Il existe un unique utilisateur test dont les identifiants sont définis par les constantes de classe USERNAME,
        EMAIL et PASSWORD
    - Cet utilisateur a son attribut force_logout à True
    """

    USERNAME = 'test'
    EMAIL = 'test@test.test'
    PASSWORD = '000000'

    def setUp(self) -> None:
        # Créer l'utilisateur test
        UserExtended.objects.create_user(self.USERNAME, self.EMAIL, self.PASSWORD)
        user = UserExtended.objects.get(username=self.USERNAME)
        user.force_logout = True
        user.save()

    def tearDown(self) -> None:
        # Supprimer l'utilisateur test
        user = UserExtended.objects.get(username=self.USERNAME)
        user.delete()

    def test_login_user_disable_force_logout(self):
        """
        Test Case : lorsqu'un utilisateur se connecte, son attribut force_logout doit passer à False
        > Vérifie que le force_logout de l'utilisateur est bien à False après sa connexion
        """
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
        user = UserExtended.objects.get(username=self.USERNAME)
        self.assertFalse(user.force_logout)


### Tests sur les middlewares


class ForceLogoutMiddlewareTestCase(TestCase):
    """
    Pas de règles d'entrée et de sortie pour cette classe de test
    """

    URL_DEFAULT = 'accueil:index'

    USERNAME = 'test'
    EMAIL = 'test@test.test'
    PASSWORD = '000000'

    def test_disconnect_if_force_logout(self):
        """
        Test Case : si un utilisateur se fait activer son force_logout, alors il doit être déconnecté à la requête
        suivante et un message de warning doit s'afficher
        > Vérifie que l'utilisateur est bien déconnecté au chargement d'une page avec son force_logout à True
        > Vérifie que l'utilisateur est redirigé après sa déconnexion (code 302)
        > Vérifie que le bon message informatif est envoyé à la suite de la déconnexion
        """
        UserExtended.objects.create_user(self.USERNAME, self.EMAIL, self.PASSWORD)
        self.client.login(username=self.USERNAME, password=self.PASSWORD)

        user = UserExtended.objects.get(username=self.USERNAME)
        user.force_logout = True
        user.save()

        response = self.client.get(reverse_lazy(self.URL_DEFAULT))
        client_user = get_user(self.client)
        self.assertFalse(client_user.is_authenticated)
        self.assertEqual(response.status_code, 302)

        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn(MESSAGE_WARNING_FORCED_LOGOUT, messages)
