# Generated by Django 3.1.3 on 2020-12-22 12:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_usercreationtoken'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userextended',
            name='bieres_en_stock',
        ),
    ]
