from django.contrib import messages
from django.contrib.auth import logout


MESSAGE_WARNING_FORCED_LOGOUT = "Votre session a expiré en raison de changements dans vos permissions " \
                                "utilisateur ou du déploiement d'une nouvelle version du logiciel. " \
                                "Merci de vous reconnecter."


class ForceLogoutMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated and request.user.force_logout:
            logout(request)
            messages.warning(request, MESSAGE_WARNING_FORCED_LOGOUT)
        response = self.get_response(request)
        return response