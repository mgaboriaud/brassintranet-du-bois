from django.urls import path

from . import views

app_name = 'users'

urlpatterns = [

    # Authentification
    path('login/', views.user_login, name="login"),
    path('logout/', views.user_logout, name="logout"),

    # Panneau utilisateur
    path('profil/', views.user_panel, name="profil"),
    path('profil/<str:submitted_data_form>', views.user_panel, name="profil_post"),

    # Inscription
    path('inscription/<key>', views.inscription, name="inscription"),

    # Changement du mdp
    path('changer-mdp/', views.ChangerMdpView.as_view(), name="changer_mdp"),
    path('changer-mdp/success', views.changer_mdp_success, name="changer_mdp_success"),

    # Mdp oublié ?
    path('reset-mdp/', views.ResetMdpView.as_view(), name = "reset_mdp"),
    path('reset-mdp/done', views.ResetMdpDoneView.as_view(), name="reset_mdp_done"),
    path('reset-mdp/formulaire-mdp/<uidb64>/<token>/', views.ResetMdpFormView.as_view(), name="reset_mdp_formulaire"),
]