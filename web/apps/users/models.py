import random
import urllib.parse

from django.contrib.auth.signals import user_logged_out, user_logged_in
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import pre_save, m2m_changed
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils import timezone

from .tasks import send_async_email_html

import string
from datetime import datetime, timedelta
from hashlib import sha256
from uuid import uuid4


# Additional methods


def generate_token_key():
    """
    Génère une clé de jeton d'inscription selon les règles de format imposées par la classe UserCreationToken
    @return: la clé du jeton d'inscription sous forme de chaîne de caractères
    """
    return ''.join(random.choice(UserCreationToken.KEY_CHARACTERSET) for _ in range(UserCreationToken.KEY_LENGTH))


def generate_digest_from_key_and_salt(key, salt):
    """
    Retourne un digest hexadécimal à partir de la clé et du salt donnés un argument.
    Le digest est généré sous la forme SHA256(0x<salt> + 0x<key>)
    @param key: chaîne de caractères à partir de laquelle sera généré le digest
    @param salt: nombre hexadécimal permettant d'introduire du bruit dans le calcul du digest
    @return: digest de la clé et du salt sous forme de nombre hexadécimal
    """
    hash_object = sha256(salt.encode() + key.encode())
    return hash_object.hexdigest()


# Model Managers


class UserCreationTokenUnexpiredManager(models.Manager):
    """
    Gestionnaire d'objets pour le modèle UserCreationToken.
    Se base sur le QuerySet des jetons n'ayant pas atteint expiration
    """
    def get_queryset(self):
        """
        Surcharge de la méthode get_queryset
        Permet de retourner le QuerySet de tous les jetons sauf ceux ayant expiré
        """
        return UserCreationToken.objects.filter(date_expiration__gt=timezone.now())

    def retrieve_token_with_key(self, key, from_url):
        """
        Renvoie le jeton d'inscription ayant la clé correspondante s'il existe. Prend en compte l'encodage de la clé
        si elle vient directement d'une URL
        @param key: clé qui a permis de générer le jeton d'inscription que l'on souhaite récupérer
        @param from_url: booléen, à mettre à True si key vient d'une URL
        @return: l'objet UserCreationToken qui a été généré avec la clé donnée en paramètre, ou None si aucun jeton
            correspondant n'a été trouvé.
        """
        if from_url:
            key = urllib.parse.unquote(key)
        for token in self.all():
            # Générer un hexdigest à partir de la clé et du salt du jeton courant
            digest, salt = token.hash.split(':')

            # Retourner le jeton courant si le hexdigest généré correspond à celui enregistré en base
            if generate_digest_from_key_and_salt(key, salt) == digest:
                return token

        # Retourner None si rien n'a été trouvé
        return None


# Model classes


class UserExtended(AbstractUser):
    """
    Représente l'utilisateur dans notre application
    """
    first_name = models.CharField(
        verbose_name="Prénom",
        max_length=150,
        blank=False
    )
    last_name = models.CharField(
        verbose_name="Nom",
        max_length=150,
        blank=False
    )
    email = models.EmailField(
        unique=True
    )
    couleur = models.CharField(
        verbose_name="Couleur",
        max_length=7,
        default='#ffffff'
    )
    force_logout = models.BooleanField(
        verbose_name="Déconnecter l'utilisateur à son prochain accès au site",
        default=False,
        null=False,
        blank = False
    )


class UserCreationToken(models.Model):
    """
    Représente le jeton d'inscription nécessaire à la création d'un nouveau compte.
    """
    KEY_LENGTH = 32
    KEY_CHARACTERSET = string.ascii_letters + string.digits + "$-_.+!*‘(),"
    # Attention : à chaque changement de cette valeur, mettre à jour la valeur du délai d'expiration dans le snippet
    # admin_token_instructions.html
    TOKEN_EXPIRATION_DELAY = {
        'hours': 0,
        'minutes': 15,
        'seconds': 0
    }

    # Clé du jeton d'inscription que l'utilisateur devra entrer dans le formulaire pour accéder à l'interface
    # d'inscription.
    key = models.CharField(
        verbose_name="Clé du jeton d'inscription",
        max_length=32,
        null=True
    )
    # Hash du jeton stocké en base (le hash de la clé donnée par l'utilisateur sera comparé avec cette valeur en base)
    hash = models.CharField(
        max_length=128,
        null=True,
        editable=False
    )
    # Date d'expiration du jeton
    date_expiration = models.DateTimeField(
        verbose_name="Date d'expiration du jeton",
        default=timezone.now,
        editable=False
    )
    # Email du destinataire à qui on enverra l'URL d'inscription contenant le jeton en argument
    recipient_email = models.EmailField(
        verbose_name="Mail de destination du lien d'inscription",
        max_length=254,
        null=True,
    )

    objects = models.Manager()
    objects_unexpired = UserCreationTokenUnexpiredManager()

    class Meta:
        verbose_name = "jeton d'inscription"
        verbose_name_plural = "jetons d'inscription"

    def generate_hash_from_key(self):
        """
        Permet de générer la valeur de l'attribut hash en fonction de l'attribut key
        La valeur du hash est de la forme "SHA256(0x<salt> + 0x<key>):<salt>"
        @return: chaîne de caractères contenant le hash du jeton d'inscription généré
        """
        salt = uuid4().hex
        return generate_digest_from_key_and_salt(self.key, salt) + ':' + salt

    def get_expiration_timedelta(self):
        """
        Calcule et renvoie l'objet timedelta représentant le délai d'expiration du jeton à partir du dictionnaire de constantes
        TOKEN_EXPIRATION_DELAY
        @return: délai d'expiration du jeton dans un objet timedelta
        """
        return timedelta(
            hours=self.TOKEN_EXPIRATION_DELAY['hours'],
            minutes=self.TOKEN_EXPIRATION_DELAY['minutes'],
            seconds=self.TOKEN_EXPIRATION_DELAY['seconds'],
        )

    def save(self, *args, **kwargs):
        """
        Surcharge de la méthode save.
        Lors de la création d'un nouveau jeton, on génère son hash stocké en base ainsi que sa date d'expiration,
            et on supprime toute trace de sa clé de génération.
        """
        if self.pk is None:
            self.key = generate_token_key()
            self.hash = self.generate_hash_from_key()
            self.date_expiration = timezone.now() + self.get_expiration_timedelta()
            send_async_email_html.delay(
                "Inscription staff Brassintranet du Bois",
                render_to_string('users/email/email_inscription.html', context={'key': self.key}),
                [self.recipient_email]
            )
        self.key = None
        super().save(*args, **kwargs)


# Model signal listeners


@receiver(pre_save, sender=UserExtended)
def notify_staff_on_new_staff_or_admin_user(sender, instance, **kwargs):
    """
    Notifie par mail les membres du staff qu'un compte admin ou staff a été créé en spécifiant son nom d'utilisateur
    > Signal : Ajout d'un enregistrement dans la table du modèle UserExtended
    """
    notify_staff = False
    try:
        # Si l'utilisateur existe déjà on compare les changements de statut staff et superuser
        user_updating = UserExtended.objects.get(id=instance.id)
        notify_staff = (instance.is_staff and not user_updating.is_staff) \
                       or (instance.is_superuser and not user_updating.is_superuser)
    except:
        # Sinon on regarde juste son statut staff et superuser
        notify_staff = instance.is_staff or instance.is_superuser

    if notify_staff:
        send_async_email_html.delay(
            "Un nouvel administrateur parmi nous : " + instance.username,
            render_to_string('users/email/email_notify_new_admin.html', context={'username': instance.username}),
            [user.email for user in UserExtended.objects.filter(is_staff=True)]
        )


@receiver(m2m_changed, sender=UserExtended.user_permissions.through)
def enable_force_logout_on_permission_change(sender, instance, **kwargs):
    """
    Met l'attribut force_logout de l'utilisateur courant à True lorsque ses permissions changent
    > Signal : Ajout ou suppression d'une permission dans la liste des permissions utilisateur
    """
    instance.force_logout = True
    instance.save()


@receiver(user_logged_in)
def disable_force_logout_on_connection(sender, user, request, **kwargs):
    """
    Met l'attribut force_logout de l'utilisateur courant à False lorsqu'il se connecte
    > Signal : Connexion d'un utilisateur
    """
    user.force_logout = False
    user.save()


@receiver(user_logged_out)
def clear_cache_on_logout(sender, user, request, **kwargs):
    """
    Supprime les variables de session custom (stockées en cache) lorsque l'utilisateur se déconnecte
    > Signal : Déconnexion d'un utilisateur
    """
    if 'navbar_data' in request.session:
        del request.session['navbar_data']
    if 'version' in request.session:
        del request.session['version']