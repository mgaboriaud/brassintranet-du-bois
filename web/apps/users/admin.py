from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.http import Http404
from django.template.loader import render_to_string

from .models import UserExtended, UserCreationToken


@admin.register(UserExtended)
class UserExtendedAdmin(UserAdmin):
    model = UserExtended
    # Attributs à faire apparaître dans le tableau des utilisateurs
    list_display = (
        'username',
        'first_name',
        'last_name',
        'email',
        'is_active',
        'is_staff',
        'is_superuser',
    )
    # Structure du formulaire de création/modification
    fieldsets = (
        (None, {
            'fields': (
                'username',
            )
        }),
        ('Personal info', {
            'fields': (
                'first_name',
                'last_name',
                'email',
            )
        }),
        ('Permissions', {
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
                'groups',
                'user_permissions'
            )
        }),
        ('Important dates', {
            'fields': (
                'last_login',
                'date_joined'
            )
        }),
    )

    def user_change_password(self, request, id, form_url=''):
        """
        Surcharge de la vue de reset de mdp depuis l'administration
        Empêche qu'un administrateur change le mot de passe d'un autre administrateur
        """
        raise Http404


@admin.register(UserCreationToken)
class UserCreationTokenAdmin(admin.ModelAdmin):
    TOKEN_INSTRUCTIONS = render_to_string('users/snippets/admin_token_instructions.html')

    # Attributs à faire apparaître dans le tableau des utilisateurs
    list_display = (
        'key',
        'hash',
        'date_expiration',
        'recipient_email'
    )
    # Structure du formulaire de création/modification
    fieldsets = (
        ("Email de destination du lien d'inscription", {
            "classes": ['wide', ],
            "description": TOKEN_INSTRUCTIONS,
            "fields": ('recipient_email',)
        }),
    )