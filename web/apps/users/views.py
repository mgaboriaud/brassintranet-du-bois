import copy

from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.views import PasswordResetDoneView, PasswordChangeView, PasswordResetConfirmView, \
    PasswordResetView
from django.db import transaction
from django.http import Http404
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy

from brassintranet import settings
from brassintranet.settings import LOGIN_REDIRECT_URL, EMAIL_HOST_USER
from .forms import LoginForm, TokenSendForm, InscriptionForm, ProfilForm, CouleurForm
from .models import UserExtended, UserCreationToken
from .tasks import send_async_email_html


# Constantes


MESSAGE_INFO_MDP_RESET_OK = "Veuillez vérifier vos emails. Si l'adresse que vous avez renseignée correspond à un " \
                            "compte existant, vous allez recevoir un lien permettant de réinitialiser votre mot " \
                            "de passe."

MESSAGE_SUCCESS_MDP_CHANGE_OK = "Votre mot de passe a été modifié avec succès !"
MESSAGE_SUCCESS_PROFIL_CHANGE_OK = "Votre profil a été modifié avec succès !"
MESSAGE_SUCCESS_SIGNUP_OK = "Votre compte a été créé avec succès !"
MESSAGE_SUCCESS_TOKEN_KEY_OK = "Votre lien d'inscription est conforme. Veuillez vous inscrire via le formulaire " \
                               "ci-dessous."

MESSAGE_WARNING_MDP_RESET_OK = "Pensez à vérifier vos courriers indésirables ! L'email de réinitialisation du mot de " \
                               "passe pourrait s'y trouver."

MESSAGE_ERROR_PROFIL_FIELD_ERRORS = "Erreur : les changements n'ont pas pu être sauvegardés car certains champs sont " \
                                    "erronés. Veuillez réessayer en apportant les correctifs nécessaires."
MESSAGE_ERROR_SIGNUP_FIELD_ERRORS = "Erreur : certains champs sont erronés, veuillez apporter les correctifs " \
                                    "nécessaires puis réessayer."
MESSAGE_ERROR_WRONG_CREDENTIALS = "Vos identifiants n'ont pas été reconnus, veuillez vérifier votre mot de passe, " \
                                  "adresse mail ou nom d'utilisateur, puis réessayez"
MESSAGE_ERROR_WRONG_TOKEN_KEY = "Erreur : La clé spécifiée dans l'URL est incorrecte"


# Méthodes utilitaires


def redirect_if_authenticated(request):
    """
    Renvoie une redirection si l'utilisateur est déjà connecté, ou False sinon (permet de tester dans la vue pour
        contrôler la valeur de retour)
    """
    if request.user.is_authenticated:
        return redirect(LOGIN_REDIRECT_URL)
    else:
        return False


def raise_404_if_no_unexpired_token(request):
    """
    Renvoie une erreur 404 s'il n'existe aucun jeton d'inscription non-expiré en base
    """
    if not UserCreationToken.objects_unexpired.exists():
        raise Http404


# Vues principales


def user_login(request):
    """
    Vue de connexion de l'utilisateur. Charge le formulaire de connexion si l'utilisateur n'est pas connecté,
        redirige vers la page d'accueil sinon.
    > Liée au template users/login.html
    """
    context_vars = {}

    # Rediriger vers la page d'accueil si on est déjà connecté
    if redirect_if_authenticated(request): return redirect_if_authenticated(request)
    else:
        # Si on arrive depuis un POST (aka un formulaire de connexion a été posté), alors on traite la demande de co
        if request.method == 'POST':
            form = LoginForm(request.POST or None)
            # Commencer la procédure d'authentification si les champs du formulaires sont bien remplis
            if form.is_valid():
                username_email = form.cleaned_data['username_email']
                password = form.cleaned_data['password']

                # Essayer de connecter l'utilisateur avec son username. Si ça ne marche pas, utiliser username_email en
                # tant qu'email pour récupérer l'utilisateur s'il existe, puis l'authentifier.
                # Si ça ne marche toujours pas, utilisateur non-reconnu => on signale une erreur
                try:
                    user_connecting = authenticate(username=username_email, password=password)

                    # Si la méthode d'authentification a trouvé un couple username/mdp qui existe, alors on connecte
                    # l'utilisateur et on le redirige vers la page d'accueil par défaut.
                    if user_connecting:
                        login(request, user_connecting)
                        return redirect(LOGIN_REDIRECT_URL)
                    # Sinon, on réessaye en considérant username_email comme un email pour récupérer l'utilisateur s'il
                    # existe.
                    else:
                        user_from_provided_email = UserExtended.objects.get(email=username_email)
                        username = user_from_provided_email.username
                        user_connecting = authenticate(username=username, password=password)

                        # Si la méthode d'authentification a trouvé un couple username/mdp qui existe, alors on connecte
                        # l'utilisateur et on le redirige vers la page d'accueil par défaut.
                        if user_connecting:
                            login(request, user_connecting)
                            return redirect(LOGIN_REDIRECT_URL)
                        else:
                            messages.error(request, MESSAGE_ERROR_WRONG_CREDENTIALS)
                except:
                    messages.error(request, MESSAGE_ERROR_WRONG_CREDENTIALS)
            else:
                messages.error(request, MESSAGE_ERROR_WRONG_CREDENTIALS)
        else:
            form = LoginForm()

        context_vars['form'] = form
        context_vars['tokens_exist'] = UserCreationToken.objects_unexpired.exists()
        return render(request, 'users/login.html', context_vars)


def user_logout(request):
    """
    Vue de déconnexion de l'utilisateur.
    > Redirige vers la page d'accueil et ainsi automatiquement vers la page de connexion
    """
    logout(request)
    return redirect(LOGIN_REDIRECT_URL)


@transaction.atomic
@staff_member_required(login_url=settings.LOGIN_URL)
def user_panel(request, submitted_data_form=''):
    """
    Vue chargeant la section des informations utilisateur
    """
    context_vars = {}
    form_in_profil = ProfilForm()  # form_in = formulaire côté interface
    form_in_couleur = CouleurForm()

    if request.method == 'POST':
        # On utilise une copie de l'utilisateur pour éviter de commit des données partiellement fausses
        buffer_user = copy.copy(request.user)

        # Récupération du formulaire en fonction du paramètre qui indique de quel formulaire on récupère les données
        if submitted_data_form == 'profil':
            form_out = ProfilForm(request.POST, instance=buffer_user)  # form_out = données du formulaire qu'on récupère
            # Formulaire à qui transmettre les erreurs = le même utilisé pour transmettre les données
            form_to_transmit_errors_to = form_in_profil
        elif submitted_data_form == 'couleur':
            form_out = CouleurForm(request.POST, instance=buffer_user)
            form_to_transmit_errors_to = form_in_couleur
        else:
            raise Http404

        if form_out.is_valid():
            # On ne modifie l'instance que si le formulaire est valide
            form_out.save()
            messages.success(request, MESSAGE_SUCCESS_PROFIL_CHANGE_OK)
        else:
            # On ne transmet que les erreurs reçues au formulaire d'entrée nouvellement chargé
            form_to_transmit_errors_to._errors = form_out._errors
            messages.error(request, MESSAGE_ERROR_PROFIL_FIELD_ERRORS)

        context_vars['form_out'] = form_out  # Pour vérifier le type de form_out dans les tests unitaires

    context_vars['form_profil'] = form_in_profil
    context_vars['form_couleur'] = form_in_couleur
    return render(request, 'users/user_panel.html', context_vars)


@transaction.atomic
def inscription(request, key):
    """
    Vue ayant les 2 fonctions suivantes :
    - Réceptionner la clé du jeton d'inscription et diriger le visiteur vers la page d'inscription si le jeton est
        reconnu.
    - Créer un compte administrateur en fonction des données entrées dans le formulaire
    > Liée au template users/inscription.html,
    > Charge le template users/token_fail.html si la clé du jeton présente dans les données de la requête POST n'a
        pas été reconnue
    > Charge le template users/inscription_réussie.html si un formulaire d'inscription valide a été envoyé
    """
    context_vars = {}

    # Retourner une erreur 404 s'il n'existe aucun token non-expiré en base
    raise_404_if_no_unexpired_token(request)
    # Rediriger vers la page d'accueil si on est déjà connecté
    if redirect_if_authenticated(request): return redirect_if_authenticated(request)
    else:
        # Récupération du jeton d'inscription depuis l'URL
        token = UserCreationToken.objects_unexpired.retrieve_token_with_key(key, True)
        # Si la clé du jeton que l'on a entré dans l'URL est valide, on charge la page d'inscription.
        if token:
            # Si cette vue a été rechargée depuis le formulaire d'inscription (ce qui veut dire qu'on a envoyé des
            # données par le biais du formulaire utilisateur) on lance la procédure de création d'utilisateur
            if request.method == 'POST':
                form = InscriptionForm(request.POST or None)

                # Si les données du formulaire sont valides, on crée l'utilisateur et on supprime le jeton
                if form.is_valid():
                    new_user = form.save(commit=False)
                    new_user.is_staff = True
                    new_user.save()

                    token.delete()

                    send_async_email_html.delay(
                        "Bienvenue sur le Brassintranet du Bois !",
                        render_to_string('users/email/email_welcome.html', context={'username': new_user.username}),
                        [new_user.email]
                    )
                    messages.success(request, MESSAGE_SUCCESS_SIGNUP_OK)
                    return render(request, 'users/notification_and_homebutton.html', context_vars)
                # Sinon, on recharge la page d'inscription en spécifiant les erreurs
                else:
                    messages.error(request, MESSAGE_ERROR_SIGNUP_FIELD_ERRORS)
            # Si c'est notre première visite sur le formulaire, on fournit tout simplement un formulaire vide avec
            # l'email pré-rempli (récupéré dans les données du jeton)
            else:
                messages.success(request, MESSAGE_SUCCESS_TOKEN_KEY_OK)
                form = InscriptionForm(initial={'email': token.recipient_email})

            context_vars['form'] = form
            context_vars['key'] = key
            return render(request, 'users/inscription.html', context_vars)
        # Sinon, on retourne la page d'erreur
        else:
            messages.error(request, MESSAGE_ERROR_WRONG_TOKEN_KEY)
            return render(request, 'users/token_fail.html')


def changer_mdp_success(request):
    """
    Vue affichant un message de succès de changement de mot de passe
    > Charge le template users/notification_and_homebutton.html avec un message de succès
    """
    context_vars = {}

    messages.success(request, MESSAGE_SUCCESS_MDP_CHANGE_OK)
    return render(request, 'users/notification_and_homebutton.html', context_vars)


# Vues génériques


class ChangerMdpView(PasswordChangeView):
    template_name = 'users/changer_mdp.html'
    success_url = reverse_lazy('users:changer_mdp_success')


class ResetMdpView(PasswordResetView):
    template_name = 'users/reset_mdp.html'
    subject_template_name = 'users/email/email_reset_password.txt'
    html_email_template_name = 'users/email/email_reset_password.html'
    email_template_name = 'users/email/email_reset_password.html'
    success_url = reverse_lazy('users:reset_mdp_done')


class ResetMdpDoneView(PasswordResetDoneView):
    template_name = 'users/notification_and_homebutton.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        messages.info(request, MESSAGE_INFO_MDP_RESET_OK)
        messages.warning(request, MESSAGE_WARNING_MDP_RESET_OK)
        return self.render_to_response(context)


class ResetMdpFormView(PasswordResetConfirmView):
    template_name = 'users/reset_mdp_formulaire.html'
    success_url = reverse_lazy('users:changer_mdp_success')
