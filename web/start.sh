#!/bin/sh

# Préparation des assets statiques et de la base de données
python manage.py collectstatic --no-input
python manage.py migrate --no-input

# Lancement de l'écoute HTTP par gunicorn (hot-reload si DEV_MODE = true)
if [ "$DEV_MODE" = "true" ]; then
  gunicorn brassintranet.wsgi:application --bind 0.0.0.0:8000 --log-level=info --reload
else
  gunicorn brassintranet.wsgi:application --bind 0.0.0.0:8000
fi