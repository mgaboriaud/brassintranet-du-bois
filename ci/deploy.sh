#!/bin/bash

### Méthodes utilitaires

tprint() {
    timestamp=`date +'[%T.%4N]'`
    echo "\e[1;33mBDB\e[0m" $timestamp "$1"
}

### Script principal

set -e

echo "\e[1m----- \e[1;33mBrassintranet du Bois\e[0m -----\e[0m"
sleep .1
echo "\e[33mPipeline de déploiement\e[0m \n"
sleep .5

# Arrêt des conteneurs
tprint "Arrêt des conteneurs en arrière-plan..."
docker-compose down

# Récupération des dernières sources du dépôt
tprint "Récupération des sources du dépôt depuis la branche master..."
git pull
tprint "Sources à jour !"

# Construction des images docker
tprint "Construction des images Docker..."
docker-compose build

tprint "Déploiement terminé. Les sources ont été mises à jour avec succès."

# Fermeture de toutes les sessions utilisateur pour forcer leur déconnexion
tprint "Fermeture de toutes les sessions utilisateur..."
docker-compose run --rm web python manage.py reset_sessions
tprint "Toutes les sessions ont été réinitialisées avec succès."

# Lancement des images docker
tprint "Lancement de l'application..."
docker-compose up -d