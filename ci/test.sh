#!/bin/bash

### Méthodes utilitaires

tprint() {
    timestamp=`date +'[%T.%4N]'`
    echo -e "\e[1;33mBDB\e[0m" $timestamp "$1"
}

### Script principal

set -e

echo -e "\e[1m----- \e[1;33mBrassintranet du Bois\e[0m -----\e[0m"
sleep .1
echo -e "\e[33mPipeline de test\e[0m \n"
sleep .5

# Installer docker-compose sur le runner docker (alpine based)
tprint "Installation de Docker Compose..."
apk add --no-cache docker-compose
tprint "Installation réussie."

# Construire les fichiers de variables d'environnement
tprint "Définition des variables d'environnement..."
cp ./env/django.env.example ./env/django.env
cp ./env/db.env.example ./env/db.env

# Lancer les tests sur une image web temporaire
tprint "Construction des images Docker..."
docker-compose build postgres
docker-compose up -d postgres
docker-compose build web
tprint "Mise à jour des fichiers statiques..."
docker-compose run --rm web python manage.py collectstatic
tprint "Lancement des tests unitaires..."
docker-compose run --rm web python manage.py test