#!/bin/bash

### Méthodes utilitaires

tprint() {
    timestamp=`date +'[%T.%4N]'`
    echo "\e[1;33mBDB\e[0m" $timestamp "$1"
}

exitwerr() {
    echo "\e[31mErreur :\e[0m" "$1"
    exit 1
}

### Script principal

set -e

echo "\e[1m----- \e[1;33mBrassintranet du Bois\e[0m -----\e[0m"
sleep .1
echo "\e[33mBienvenue dans l'assistant d'installation\e[0m \n"
sleep .5

tprint "Démarrage de la procédure d'installation..."

# Demander l'arrêt de postgresql pour libérer le port 5432
tprint "Libération du port 5432 pour PostgreSQL..."
echo "Le port 5432 généralement utilisé par le service postgresql doit être libéré pour le bon déroulement de l'installation."
echo -n "Souhaitez-vous arrêter le service postgresql (recommandé) ? [o/n] : "
read answer_stop_postgres
case $answer_stop_postgres in
"o")
    tprint "Arrêt du daemon postgresql pour libérer le port 5432..."
    systemctl stop postgresql || tprint "\e[31mATTENTION :\e[0m Le service postgresql n'a pas pu être arrêté ou n'existe pas sur cet environnement."
    ;;
*)
    tprint "Le service postgresql ne sera pas arrêté. Reprise de l'installation..."
    ;;
esac

# Construire les conteneurs Docker
tprint "Démarrage du daemon docker..."
systemctl start docker
tprint "Docker a bien été lancé."
tprint "Nettoyage des conteneurs de l'application..."
docker-compose down
tprint "Construction des conteneurs Docker..."
docker-compose build

# Lancer le conteneur de base de données (nécessite le conteneur des sources)
tprint "Construction terminée."
tprint "Démarrage du conteneur \e[1;32mweb\e[0m pour la configuration de la base de données..."
docker-compose up -d web 
tprint "La configuration de la base de données va démarrer..."
tprint "Délai d'attente estimé à 15s."
sleep 15

# Une fois dedans, initialiser la BDD
tprint "Application des migrations de la base de données..."
docker exec -it brassintranet-du-bois_web_1 python manage.py migrate
tprint "Migration terminée."

# Lancer le script de création de superuser
tprint "Lancement de la création du superutilisateur..."
echo "Le superutilisateur est l'administrateur et premier utilisateur de l'application."
echo "\e[32mVeuillez entrer les identifiants du superutilisateur ci-dessous\e[0m"
echo "\e[31mATTENTION :\e[0m \e[1mPour une installation en production, veuillez choisir un mot de passe sécurisé au maximum 
    (long mot de passe avec minusucles, majuscules, chiffres et symboles)\e[0m"

docker exec -it brassintranet-du-bois_web_1 python manage.py createsuperuser
tprint "Le superutilisateur a bien été créé avec les identifiants donnés."

# Arrêt des conteneurs ouverts pour la configuration
tprint "Arrêt des conteneurs..."
docker-compose down

# Donner à l'utilisateur les instructions de lancement de l'application (docker-compose up (-f production.yml))
tprint "Fin de l'installation."
echo "\e[1;32mInstallation terminée avec succès.\e[0m"
echo "Le Brassintranet du Bois a bien été installé sur votre machine."