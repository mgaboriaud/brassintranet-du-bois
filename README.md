Le **Brassintranet du Bois** est l'application de gestion de la Brasserie du Bois à laquelle seuls les membres du collège de l'association disposent d'un accès.

# Fonctionnalités

Le projet est toujours en cours de développement, ainsi certaines de ces fonctionnalités sont à venir.

- Tableau de bord résumant l'activité de la brasserie
- Calendrier des événements de l'association
- Répertoire de recettes
- Gestionnaire de stocks (bières et ingrédients)
- Gestionnaire de brassins
- Gestionnaire de matériel (cuves, fûts, bouteilles)
- Gestionnaire de commandes
- Gestionnaire administratif de l'association

# Stack technique

Le projet repose sur le framework web **Django** pour Python. Ses sources sont servies par **Gunicorn** et les requêtes HTTP orchestrées par **NGINX**.

La persistence des données des gérée au sein d'une base **PostgreSQL**.

L'application repose également sur les services **Redis** et **Celery** pour la gestion des tâches asynchrones.

Enfin, l'ensemble des services de l'application tournent au sein de conteneurs **Docker** orchestrés par **Docker Compose**, et les sources sont servies en continu via la fonctionnalité **CI/CD de GitLab**.

# Installation locale

La procédure d'installation se trouve dans [cet article du wiki](https://gitlab.com/mgaboriaud/brassintranet-du-bois/-/wikis/dev/Installation-locale-et-lancement-de-l'application).

Pour plus d'informations sur l'utilisation de l'application ainsi que des aides au développement, nous vous invitons à consulter le reste du [wiki](https://gitlab.com/mgaboriaud/brassintranet-du-bois/-/wikis/home).
