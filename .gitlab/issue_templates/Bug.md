# Description du bug

### Version du BDB

*compléter*

### Comportement attendu

*compléter*

### Comportement observé

*compléter*

# Etapes de reproduction

- *Détailler sous forme de liste la succession des actions qui mènent à ce bug*

# Pistes de résolution

*compléter, facultatif*

# Workflow

- [ ] ***facultatif, compléter avec des tâches***
    - [ ] *facultatif, compléter avec des sous-tâches*

- [ ] Changelog release