# Nouveaux articles

*compléter selon le modèle ci-dessous*

### **USER/DEV/DJANGO/DOCKER : Nom de l'article**

*compléter avec plan détaillé*

- *titre 1*
    - *sous-titre 1.1*

# Liens à établir

*établir une liste de liens à effectuer depuis ou vers des articles extérieurs*

- [ ] *compléter*

# Workflow

- [ ] ***Nom de l'article***
    - [ ] *tâche 1*
    - [ ] *tâche 2*

- [ ] Changelog release