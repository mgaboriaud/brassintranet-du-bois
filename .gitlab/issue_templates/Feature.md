# Description de la fonctionnalité

*compléter*

# Spécifications

*ex : use cases, exigences fonctionnelles, IHM (au choix : croquis / description détaillée / maquette détaillée), modèle de données, détail d'un algorithme...*

### Nom de la spec *(exemples ci-dessus entre parenthèses)*

*compléter*

# Workflow

### Développement

- [ ] ***compléter avec des tâches***
    - [ ] *compléter avec des sous-tâches*

### Test

- [ ] Doc utilisateur
    - [ ] *sous-tâche doc*
- [ ] Tests unitaires
- [ ] Changelog release