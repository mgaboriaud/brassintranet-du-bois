#!/bin/sh

# Création de la base de données
psql -U postgres -c "CREATE DATABASE $DB_NAME;"

# Création et configuration de l'utilisateur
psql -U postgres -c "CREATE USER $DB_USER WITH ENCRYPTED PASSWORD '$DB_PASS';"
psql -U postgres -c "ALTER ROLE $DB_USER SET client_encoding TO 'utf8';"
psql -U postgres -c "ALTER ROLE $DB_USER SET default_transaction_isolation TO 'read committed';"
psql -U postgres -c "ALTER ROLE $DB_USER SET timezone TO '$DB_TIMEZONE';"

# Réglage des permissions de l'utilisateur (permet de pouvoir créer une base de test)
psql -U postgres -c "ALTER USER $DB_USER CREATEDB;"

# Application des privilèges de la base de données à l'utilisateur
psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;"
